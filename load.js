import CustomError from './app/lib/CustomError'
require('dotenv').config();

global.__DEV__ = process.env.NODE_ENV !== 'production'
global.__PORT__ = process.env.PORT || 3000
global.CustomError = CustomError
