import './load'
import Koa from 'koa'
import send from 'koa-send'
import serve from 'koa-static'
import koaLogger from 'koa-logger'
import koaBody from 'koa-body'
import cors from '@koa/cors'
import connectToDB from './app/db/connect'
import router from './app/web/routes'
import Middlewares from './app/lib/middlewares'

const koa = new Koa()

const mode = __DEV__? "dev" : "prod";
connectToDB(mode)


router.get('/', async (ctx, next) => {
  await send(ctx, './build/index.html')
})

if(__DEV__) {
  koa.use(cors())
  koa.use(koaLogger());
}
koa.use(serve('build'))
koa.use(Middlewares.upload_path(__dirname))
koa.use(koaBody())
koa.use(router.routes())
koa.use(router.allowedMethods())

if (!module.parent) {
  var server = koa.listen(__PORT__, () => {
    console.log(`server running on port ${__PORT__}`);
  });
}

global.LOG = (message, ...args) => {
  if(!__DEV__) return;

  console.log('/ ---------------'+ message +'------------------ ');
  console.log(...args);
  console.log(' ----------------------------------------------- / ');
}
