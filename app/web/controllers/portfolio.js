import Lib from '../../lib'
import Accounts from '../../contexts/accounts'
import Portfolios from '../../contexts/portfolios'
import PortfolioView from '../views/portfolio'

async function main(ctx, next) {
  try {
    const admin = await Accounts.get_admin()
    const portfolio = await Portfolios.get_portfolio(admin)
    PortfolioView.render(ctx, portfolio)
  } catch (e) {
    LOG('PortfolioController:main exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function show(ctx, next) {
  const username = ctx.params.username
  try {
    const user = await Accounts.get_user(username)
    const portfolio = await Portfolios.get_portfolio(user)
    Portfolios.render(ctx, portfolio)
  } catch (e) {
    LOG('PortfolioController:show exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function anonymouse_message(ctx, next) {
  const username = ctx.params.username
  const body = ctx.request.body
  try {
    const result = await Portfolios.create_anonymouse_message(username, body)
  } catch (e) {
    LOG('PortfolioController:show exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

export default {
  main,
  show,
  anonymouse_message,
}
