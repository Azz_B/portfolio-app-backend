import Lib from '../../lib'
import Accounts from '../../contexts/accounts'
import Blogs from '../../contexts/blogs'
import PostView from '../views/post'
import CommentView from '../views/comment'


async function index(ctx, next) {
  let { limit, offset } = ctx.query
  const logged_user_id = ctx.state.user && ctx.state.user.id || null
  try {
    const {posts, config} = await Blogs.get_posts(logged_user_id, { limit: parseInt(limit, 10) || 10, offset: parseInt(offset, 10) || 0})
    PostView.render(ctx, posts, {config})
  } catch (e) {
    console.log(e);
    LOG('PostController:index exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}


async function show(ctx, next) {
  const id = ctx.params.id
  try {
    const post = await Blogs.get_post(id)
    PostView.render_single_post(ctx, post)
  } catch (e) {
    console.log(e);
    LOG('PostController:show exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function create(ctx, next) {
  const logged_user = ctx.state.user
  try {
    check_authorization(logged_user)
    const body = {...ctx.request.body, user_id: logged_user.id}
    const post = await Blogs.create_post(body)
    PostView.render_draft(ctx, post)
  } catch (e) {
    LOG('PostController:create exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function update(ctx, next) {
  const logged_user = ctx.state.user
  const id = ctx.params.id
  try {
    check_authorization(logged_user)
    const body = {...ctx.request.body, user_id: logged_user.id }
    const post = await Blogs.update_post(id, body)
    PostView.render_draft(ctx, post)
  } catch (e) {
    LOG('PostController:update exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function remove(ctx, next) {
  const logged_user = ctx.state.user
  const id = ctx.params.id
  try {
    check_authorization(logged_user)
    await Blogs.remove_post(id)
    ctx.status = 204
  } catch (e) {
    LOG('PostController:remove exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function publish(ctx, next) {
  const logged_user = ctx.state.user
  const id = ctx.params.id
  try {
    check_authorization(logged_user)
    const post = await Blogs.publish_post(id, logged_user.id)
    PostView.render_single_post(ctx, post)
  } catch (e) {
    LOG('PostController:publish exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function comments(ctx, next) {
  const id = ctx.params.id
  try {
    const comments = await Blogs.get_post_comments(id)
    CommentView.render(ctx, comments)
  } catch (e) {
    LOG('PostController:comments exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function comment_create(ctx, next) {
  const logged_user = ctx.state.user
  const id = ctx.params.id
  try {
    check_authorization(logged_user)
    const body = {...ctx.request.body, user_id: logged_user.id, post_id: id}
    console.log(body);
    const comment = await Blogs.create_comment(body)
    CommentView.render(ctx, comment)
  } catch (e) {
    LOG('PostController:comment_create exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

function check_authorization(logged_user) {
  if(!logged_user) throw new CustomError('BlogsError', 'Action not Authorized', {}, 401)
}

export default {
  index,
  show,
  create,
  update,
  remove,
  publish,
  comments,
  comment_create,
}
