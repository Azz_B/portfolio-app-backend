import jwt from 'jsonwebtoken'
import Lib from '../../lib'
import Accounts from '../../contexts/accounts'
import AuthView from '../views/auth'
import { SECRET_JWT, jwtConfig } from '../configs'


async function login(ctx, next) {
  const { login, password } = ctx.request.body
  try {
    const user = await Accounts.authenticate_by_login_password(login, password)
    const token = jwt.sign(AuthView.auth_user(user), SECRET_JWT, jwtConfig)
    AuthView.render(ctx, user, token)
  } catch (e) {
    LOG('AuthController:login exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function login_by_token(ctx, next) {
   const token = ctx.request.headers['x-access-token'];
  try {
    if(!token) { ctx.status = 204; return; }
    let user = await new Promise((resolve, reject) => {
                  jwt.verify(token, SECRET_JWT, (err, user) => {
                    resolve(user)
                  })
                });
    user = await Accounts.get_user((user || {}).username)
    AuthView.render(ctx, user)
  } catch (e) {
    LOG('AuthController:login_by_token exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function register(ctx, next) {
  const body = ctx.request.body
  try {
    const user = await Accounts.create_user(body)
    const token = jwt.sign(AuthView.auth_user(user), SECRET_JWT, jwtConfig)
    AuthView.render(ctx, user, token)
  } catch (e) {
    LOG('AuthController:register exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

export default {
  login,
  login_by_token,
  register,
}
