import Lib from '../../lib'
import Storage from '../../lib/storage'
import Accounts from '../../contexts/accounts'
import Blogs from '../../contexts/blogs'
import UserView from '../views/user'
import PostView from '../views/post'


async function show(ctx, next) {
  const username = ctx.params.username
  const logged_user_id = ctx.state.id
  try {
    const user = await Accounts.get_user(username, logged_user_id)
    UserView.render(ctx, user)
  } catch (e) {
    LOG('UserController:show exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function update(ctx, next) {
  const logged_user = ctx.state.user
  const body = ctx.request.body
  console.log(body);
  try {
    check_authorization(logged_user)
    const user = await Accounts.update_user(logged_user.id, body)
    UserView.render(ctx, user)
  } catch (e) {
    LOG('UserController:update exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function posts(ctx, next) {
  const username = ctx.params.username
  try {
    const posts = await Blogs.get_user_posts(username)
    PostView.render(ctx, posts)
  } catch (e) {
    LOG('UserController:posts exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function photo(ctx, next) {
  const logged_user = ctx.state.user
  try {
    check_authorization(logged_user)
    let user = await Accounts.get_user_by_id(logged_user.id)
    await Storage.upload(ctx, next)
    // const file_to_remove = user.profile.photo
    user.profile.photo = ctx.req.file.filename ;
    await user.save()
    user = await Accounts.get_user(user.credential.username)
    UserView.render(ctx, user)
  } catch (e) {
    LOG('UserController:photo exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

async function display_photo(ctx, next) {
  const username = ctx.params.username
  try {
    const user = await Accounts.get_user(username)
    const { type, stream } = await Storage.stream_image(ctx.context.upload_folder_path, user.profile.photo)
    ctx.type = type
    ctx.body = stream
  } catch (e) {
    LOG('UserController:display_photo exception', e)
    await Lib.error_handler(ctx, e, next)
  }
}

function check_authorization(logged_user) {
  if(!logged_user) throw new CustomError('UserError', 'Action not Authorized', {}, 401)
  return logged_user.id
}

export default {
  show,
  update,
  posts,
  photo,
  display_photo,
}
