import Router from 'koa-router'
import Controller from '../controllers/auth'

const router = new Router({prefix: '/auth'})

router.post('/login', Controller.login)
router.get('/login_by_token', Controller.login_by_token)
router.post('/register', Controller.register)

export default router
