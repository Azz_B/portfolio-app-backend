import Router from 'koa-router'
import authRouter from './auth'
import api_v1_router from './api_v1'

const router = new Router()


router.use(authRouter.routes())
router.use(api_v1_router.routes())

export default router
