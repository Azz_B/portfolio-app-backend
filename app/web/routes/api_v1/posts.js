import Router from 'koa-router'
import Controller from '../../controllers/post'

const router = new Router({
  prefix: '/posts',
})

router.get('/', Controller.index)
router.get('/:id', Controller.show)
router.post('/', Controller.create)
router.put('/:id', Controller.update)
router.del('/:id', Controller.remove)
router.post('/:id/publish', Controller.publish)
router.get('/:id/comments', Controller.comments)
router.post('/:id/comments', Controller.comment_create)

export default router
