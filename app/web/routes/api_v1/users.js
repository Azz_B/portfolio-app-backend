import Router from 'koa-router'
import Controller from '../../controllers/user'

const router = new Router({
  prefix: '/users',
})

router.get('/:username', Controller.show)
router.put('/', Controller.update)
router.get('/:username/posts', Controller.posts)
router.post('/photo', Controller.photo)
router.get('/:username/photo', Controller.display_photo)

export default router
