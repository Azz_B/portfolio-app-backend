import Router from 'koa-router'
import jwt from 'koa-jwt'
import { SECRET_JWT } from '../../configs'
import usersRouter from './users'
import postsRouter from './posts'
import portfoliosRouter from './portfolios'

const router = new Router({
  prefix: '/api_v1',
})

router.use(
  jwt({secret: SECRET_JWT, debug: __DEV__, passthrough: true})
);

router.use(async (ctx, next) => {
  LOG('Logged user: ', ctx.state)
  await next()
})
router.use(usersRouter.routes())
router.use(postsRouter.routes())
router.use(portfoliosRouter.routes())

export default router
