import Router from 'koa-router'
import Controller from '../../controllers/portfolio'

const router = new Router({
  prefix: '/portfolios',
})

router.get('/main', Controller.main)
router.get('/:username', Controller.show)
router.post('/:username/anonymouse-message', Controller.anonymouse_message)

export default router
