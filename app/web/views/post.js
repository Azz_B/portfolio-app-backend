/*
  if a function start with render means it will render a response using ctx.body
  else it's just describe the format of data
*/

import Lib from '../../lib'


function render(ctx, data, added_data, status) {
  Lib.Tools.abstract_render(ctx, data, added_data, status)(post, 'post', 'posts')
}


/**
  this function gonna return post in this shape :
  #id
  #title
  #image
  #user -> {
    #id
    #username
    #fullname
    #photo
  }
  #likes_count
  #comments_count
  #you_liked
  #created_at
*/
function post({_id, title, image, user_creator, comments, liked_by, published_at}) {
  return {
    id: _id,
    title,
    image,
    user: {
      id: user_creator._id,
      username: user_creator.credential.username,
      fullname: user_creator.profile.fullname,
      photo: `http://localhost:4000/api_v1/users/${user_creator.credential.username}/photo`,
    },
    likes_count: liked_by.length,
    comments_count: comments.length,
    published_at,
  }
}


function render_single_post(ctx, data, added_data, status) {
  Lib.Tools.abstract_render(ctx, data, added_data, status)(single_post, 'post')
}

/**
  this function gonna return post in this shape :
  #id
  #title
  #subtitle
  #image
  #content
  #user -> {
    #id
    #username
    #fullname
    #photo
  }
  #likes_count
  #comments_count
  #you_liked
  #created_at
*/
function single_post(post_data) {
  return {
    ...post(post_data),
    content: post_data.content,
    subtitle: post_data.subtitle,
  }
}

function render_draft(ctx, data, added_data, status) {
  Lib.Tools.abstract_render(ctx, data, added_data, status)(draft_post, 'post')
}

/**
  this function gonna return draft post in this shape :
  #id
  #title_draft
  #subtitle_draft
  #content_draft
  #created_at
*/
function draft_post({_id, title_draft, subtitle_draft, content_draft, created_at}) {
  return {
    id: _id,
    title_draft, subtitle_draft, content_draft, created_at,
  }
}


export default {
  render,
  render_single_post,
  render_draft,
}
