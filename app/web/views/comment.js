/*
  if a function start with render means it will render a response using ctx.body
  else it's just describe the format of data
*/

import Lib from '../../lib'


function render(ctx, data, added_data, status) {
  Lib.Tools.abstract_render(ctx, data, added_data, status)(comment, 'comment', 'comments')
}


/**
  this function gonna return comment in this shape :
  #id
  #content
  #user -> {
    id
    username
    fullname
    photo
  }
  #replied_to
  #likes_count
  #you_liked
  #created_at
  #post_id
*/
function comment({_id, content, user_creator, liked_by, replied_to, created_at}) {
  return {
    id: _id,
    content,
    user: {
      id: user_creator._id,
      username: user_creator.credential.username,
      fullname: user_creator.profile.fullname,
      photo: user_creator.profile.photo,
    },
    replied_to,
    likes_count: liked_by.length,
    created_at,
  }
}

export default {
  render,
  comment,
}
