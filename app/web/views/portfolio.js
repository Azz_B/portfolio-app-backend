/*
  if a function start with render means it will render a response using ctx.body
  else it's just describe the format of data
*/

import Lib from '../../lib'


function render(ctx, data, added_data, status) {
  Lib.Tools.abstract_render(ctx, data, added_data, status)(portfolio, 'portfolio', 'portfolios')
}


/**
  this function gonna return portfolio in this shape :
  #id
  #intro
  #status
  #user -> {
    id
    username
  }
*/
function portfolio({_id, intro, populated_models}) {
  const { status, user } = populated_models

  return {
    id: _id,
    intro,
    status: status.content,
    user: {
      id: user._id,
      username: user.credential.username,
    },
  }
}

export default {
  render,
  portfolio,
}
