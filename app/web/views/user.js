/*
  if a function start with render means it will render a response using ctx.body
  else it's just describe the format of data
*/

import Lib from '../../lib'


function render(ctx, data, added_data, status) {
  Lib.Tools.abstract_render(ctx, data, added_data, status)(user, 'user', 'users')
}


/**
  this function gonna return user in this shape :
  #id ->
  #username ->
  #fullname ->
  #photo ->
  #bio ->
  #created_at ->
  #you_follow -> boolean | define if you're following this profile
*/
function user({_id, credential: {username}, profile: {fullname, photo, bio}, created_at}) {
  photo = `http://localhost:4000/api_v1/users/${username}/photo`
  return {
    id:_id, username, fullname, photo, bio, created_at,
  }
}

export default {
  render,
  user,
}
