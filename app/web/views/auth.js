/*
  if a function start with render means it will render a response using ctx.body
  else it's just describe the format of data
*/

import Lib from '../../lib'
import UserView from './user'

function render(ctx, user, token) {
  ctx.body = {
    user: UserView.user(user),
    auth: auth_user(user),
    token,
  }
}

function auth_user({_id, confirmed, credential: {username}, permission}) {
  return { id: _id, username, permission, confirmed }
}

export default {
  render,
  auth_user,
}
