import Portfolio from './Portfolio'
import Status from './Status'
import AnonymouseMessage from './AnonymouseMessage'
import Lib from '../../lib'
import Accounts from '../accounts'

const check_attrs = Lib.Tools.check_attrs


async function get_portfolio(user) {
  const portfolio = user.profile.portfolio
  if(!portfolio) throw new CustomError('PortfolioError', 'Portfolio not found')

  const status = await Status.findOne({portfolio: portfolio._id, selected: true})

  portfolio.populated_models = {status, user}

  return portfolio
}

async function create_anonymouse_message(username, {content}) {
  let user = await Accounts.get_user(username)
}

async function clean() {
  await AnonymouseMessage.remove({})
  await Status.remove({})
  await Portfolio.remove({})
  await Accounts.clean()
}

export default {
  get_portfolio,

  clean,
}
