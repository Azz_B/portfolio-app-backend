import mongoose, { Schema } from 'mongoose'
import Lib from '../../lib'


mongoose.Promise = global.Promise


const PortfolioSchema = new Schema({
  deleted_at: {type: Date, 'default': null},

  created_at: { type: Date, 'default': Date.now },

  intro: { type: String, required: [true, 'intro is required']},

  statuses: [{ type: Schema.Types.ObjectId, ref: 'Status' }],

  messages: [{ type: Schema.Types.ObjectId, ref: 'AnonymouseMessage' }],

  likes_received: { type: Number, 'default': 0 },
})

// -------------------------- helpers ---------------------

export function cast(params) {
  return Lib.Tools.cast(params, '')
}

const Portfolio = mongoose.model('Portfolio', PortfolioSchema)

export default Portfolio
