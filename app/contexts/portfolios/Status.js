import mongoose, { Schema } from 'mongoose'
import Lib from '../../lib'


mongoose.Promise = global.Promise


const StatusSchema = new Schema({
  deleted_at: {type: Date, 'default': null},

  created_at: { type: Date, 'default': Date.now },

  content: {type: String, required: [true, 'content is required']},

  selected: { type: Boolean, 'default': false },

  likes_received: { type: Number, 'default': 0 },

  portfolio: { type: Schema.Types.ObjectId, ref: 'Portfolio' },
})

// -------------------------- helpers ---------------------

export function cast(params) {
  return Lib.Tools.cast(params, 'content')
}

const Status = mongoose.model('Status', StatusSchema)

export default Status
