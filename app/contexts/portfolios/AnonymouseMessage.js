import mongoose, { Schema } from 'mongoose'
import Lib from '../../lib'


mongoose.Promise = global.Promise


const AnonymouseMessageSchema = new Schema({
  deleted_at: {type: Date, 'default': null},

  created_at: { type: Date, 'default': Date.now },

  content: {type: String, required: [true, 'content is required']},

  reply: String,

  replied_at: Date,
})

// -------------------------- helpers ---------------------

export function cast(params) {
  return Lib.Tools.cast(params, 'content')
}

const AnonymouseMessage = mongoose.model('AnonymouseMessage', AnonymouseMessageSchema)

export default AnonymouseMessage
