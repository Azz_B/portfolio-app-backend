import User, { cast as user_cast } from './User'
import Lib from '../../lib'

const check_attrs = Lib.Tools.check_attrs


async function get_admin() {
  const user = await User.findOne({'permission.admin': true}).populate('profile.portfolio')
  // TODO: here is big problem if we don't have an admin user -> should we throw and handle this stuation
  // NOTE: temporary Error type
  if(!user) throw new CustomError('UserError', 'User not found')

  return user
}

async function get_user(username, logged_user_id) {
  const user = await User.findOne({'credential.username': username})
  if(!user || user.deleted_at) throw new CustomError('UserError', 'User not found')

  if(logged_user_id) {
    const following = (await User.findById(logged_user_id)).profile.following
    const you_follow = (following.find(id => id === user._id))? true : false
    user.populated_fields = { you_follow }
  }

  return user
}

async function get_user_by_id(id) {
  const user = await User.findOne({_id: id, deleted_at: {$eq: null}})
  return user
}

async function create_user(attrs) {
  check_attrs(attrs, 'user')
  const {user: { username, email, password, fullname }} = attrs
  if(!password || password.length < 3) throw new CustomError('UserError', 'Invalid data', {password: 'password is required with 3 min length'}, 400)
  const user = await User.create({ credential: {username, email, password}, profile: {fullname} })
  return await get_user(user.credential.username)
}

async function update_user(user_id, attrs) {
  let user = await User.findOne({_id: user_id, deleted_at: {$eq: null}})
  check_attrs(attrs, 'user')
  const { user: user_data } = attrs
  const keys = Object.keys(user_cast(user_data))
  keys.forEach(k => user.profile[k] = user_data[k])
  user = await user.save()

  return await get_user(user.credential.username)
}


async function authenticate_by_login_password(login, password) {
  const user = await User.findOne({$or: [{'credential.email': login}, {'credential.username': login}]})
  if(!user) throw new CustomError("AuthError", "Authentication failed", {login: "this login does not exist"}, 401)
  const isMatch = await user.checkPassword(password)
  if(!isMatch) throw new CustomError("AuthError", "Authentication failed", {}, 401)

  return await get_user(user.credential.username)
}

// private
async function clean() {
  await User.remove({'credential.email': {$not: /admin@email.com/}})
}


export default {
  get_admin,
  get_user,
  get_user_by_id,
  create_user,
  update_user,
  //remove_user,

  authenticate_by_login_password,

  clean,
}
