import Post, { cast as post_cast } from './Post'
import Comment, { cast as comment_cast } from './Comment'
import Lib from '../../lib'
import Accounts from  '../accounts'

const check_attrs = Lib.Tools.check_attrs


async function get_posts(logged_user_id, {limit, offset}) {
  const [posts, count] = await Promise.all([
    Post.find({deleted_at: {$eq: null}, published_at: {$ne: null}}).populate('user_creator').limit(limit).skip(offset).exec(),
    await Post.count({deleted_at: {$eq: null}, published_at: {$ne: null}}),
  ]);

  offset += posts.length
  const config = { offset, hasMore: offset < count }
  return {posts, config}
}

async function get_post(id, is_draft) {
  const post = await Post.findOne({_id: id, deleted_at: {$eq: null}}).populate('user_creator')
  if(!post || (!is_draft && post.published_at === null)) throw new CustomError('PostError', 'Post not found')

  return post
}

async function create_post(attrs) {
  check_attrs(attrs, 'post user_id')
  const {post: post_data, user_id} = attrs
  let post = await Post.create({...post_cast(post_data), user_creator: user_id})
  return await get_post(post._id, true)
}

async function update_post(id, attrs) {
  check_attrs(attrs, 'post user_id')
  const { post: post_data, user_id } = attrs
  let post = await Post.findOne({_id: id, user_creator: user_id, deleted_at: {$eq: null}})
  if(!post) throw new CustomError('BlogsError', 'Action not Authorized', {}, 401)
  const keys = Object.keys(post_cast(post_data))
  keys.forEach(k => post[k] = post_data[k])
  post = await post.save()
  return await get_post(post._id, true)
}

async function remove_post(id, logged_user_id) {
  const post = await Post.findOne({ _id: id, user_creator: logged_user_id, deleted_at: {$eq: null} })
  if(!post) return;
  await Post.update(post, {deleted_at: Date.now()})
}

async function publish_post(id, logged_user_id) {
  let post = await Post.findOne({ _id: id, user_creator: logged_user_id, deleted_at: {$eq: null} })
  const errors = {}
  if(!post.title_draft || post.title_draft.length < 10) errors['title'] = 'title must be more than 10 characters'
  if(!post.content_draft || post.title_draft.length < 20) errors['content'] = 'content is required'
  if(Object.keys(errors) > 0) throw new CustomError('BlogsError', 'Post values is invalid', errors)
  post.title = post.title_draft
  post.subtitle = post.subtitle_draft
  post.content = post.content_draft
  if(!post.published_at) post.published_at = Date.now()
  post = await post.save()

  return get_post(post._id)
}

async function get_user_posts(username) {
  const user = await Accounts.get_user(username)
  const posts = await Post.find({$and: [{user_creator: user._id}, { deleted_at: {$eq: null}}, { published_at: {$ne: null} }]}).populate('user_creator')

  return posts
}

async function get_post_comments(id) {
  let comments = (await Post.findOne({_id: id, deleted_at: {$eq: null}, published_at: { $ne: null }}, 'comments').populate({path: 'comments', populate: {path: 'user_creator'}})).comments
  comments = comments.map(comment => {
    comment['post_id'] = id;
    return comment;
  })
  return comments
}

async function get_comment(id) {
  const comment = await Comment.findOne({_id: id, delete_at: {$eq: null}}).populate('user_creator')
  if(!comment || comment.deleted_at) throw new CustomError('CommentError', 'Comment not found')

  return comment
}

async function create_comment(attrs) {
  check_attrs(attrs, 'comment user_id post_id')
  const {comment: comment_data, user_id, post_id} = attrs
  const post = await get_post(post_id)
  let comment = await Comment.create({...comment_cast(comment_data), user_creator: user_id})
  post.comments.push(comment._id)
  await post.save()

  return await get_comment(comment._id)
}

async function update_comment(id, attrs) {
  check_attrs(attrs, 'comment user_id')
  const { comment: comment_data, user_id } = attrs
  let comment = await Comment.findOne({_id: id, user_creator: user_id, deleted_at: {$eq: null}})
  const keys = Object.keys(comment_cast(comment_data))
  keys.forEach(k => comment[k] = comment_data[k])
  comment = await comment.save()

  return await get_comment(comment._id)
}

async function remove_comment(id, logged_user_id) {
  const comment = await Comment.findOne({ _id: id, user_creator: logged_user_id })
  if(!comment) return;
  await Comment.update(comment, {deleted_at: Date.now()})
}


async function clean() {
  await Comment.remove({})
  await Post.remove({})
  await Accounts.clean()
}

export default {
  get_posts,
  get_post,
  create_post,
  update_post,
  remove_post,
  publish_post,

  get_user_posts,
  get_post_comments,

  create_comment,
  update_comment,
  remove_comment,

  clean,
}
