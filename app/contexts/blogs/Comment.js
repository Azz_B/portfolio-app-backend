import mongoose, { Schema } from 'mongoose'
import Lib from '../../lib'


mongoose.Promise = global.Promise


const CommentSchema = new Schema({
  deleted_at: {type: Date, 'default': null},

  created_at: { type: Date, 'default': Date.now },

  replied_to: { type: Schema.Types.ObjectId, ref: 'Comment' },

  content: {type: String, required: [true, 'content is required']},

  user_creator: { type: Schema.Types.ObjectId, ref: 'User' },

  liked_by: [{ type: Schema.Types.ObjectId, ref: 'User' }],

})

// -------------------------- helpers ---------------------

export function cast(params) {
  return Lib.Tools.cast(params, 'content replied_to')
}

const Comment = mongoose.model('Comment', CommentSchema)

export default Comment
