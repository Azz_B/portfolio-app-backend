import mongoose, { Schema } from 'mongoose'
import Lib from '../../lib'


mongoose.Promise = global.Promise


const PostSchema = new Schema({
  deleted_at: {type: Date, 'default': null},

  created_at: { type: Date, 'default': Date.now },

  published_at: { type: Date, 'default': null },

  title: String,

  title_draft: String,

  subtitle: String,

  subtitle_draft: String,

  image: String,

  content: String,

  content_draft: String,

  user_creator: { type: Schema.Types.ObjectId, ref: 'User' },

  liked_by: [{ type: Schema.Types.ObjectId, ref: 'User' }],

  comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
})

// -------------------------- helpers ---------------------

export function cast(params) {
  return Lib.Tools.cast(params, 'title_draft subtitle_draft content_draft')
}

const Post = mongoose.model('Post', PostSchema)

export default Post
