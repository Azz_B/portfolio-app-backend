import User from '../contexts/accounts/User'
import Portfolio from '../contexts/portfolios/Portfolio'
import Status from '../contexts/portfolios/Status'

export default async (env) => {
  if(env === 'test') { await run_test_seeds(); return; }
  if(env === 'dev') { await run_dev_seeds(); return; }

}


async function run_test_seeds() {
  //await Portfolio.remove({})
  //await User.remove({})

  const data = {confirmed: true, credential: {username: 'admin', email: 'admin@email.com', password: 'admin'}, profile: {fullname: 'admin admin', bio: 'Im the admin'}, permission: { admin: true} }
  await create_admin_user(data)
}

async function run_dev_seeds() {
  const data = {confirmed: true, credential: {username: 'AZZ-B', email: 'aikidocombat3@hotmail.com', password: 'admin'}, profile: {fullname: 'BENEKA AZZEDDINE', bio: "I'm Fullstack developer , experienced building apps using modern technolgies like React, Nodejs"}, permission: { admin: true} }
  await create_admin_user(data)
}


async function create_admin_user(data) {
  let user = await User.findOne({'credential.username': data.credential.username})
  if(user) return user

  user = await User.create(data)

  const portfolio = await Portfolio.create({intro: "Hi, I'm BENEKA AZZEDDINE Fullstack developer"})
  user.profile.portfolio = portfolio
  const status = await Status.create({portfolio: portfolio._id, content: "I'm building a blogs app", selected: true})
  await user.save()
  //console.log(user);
  return user
}

const delay = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve('ok')
  }, 200)
})
