import { expect } from 'chai';
import Accounts from '../../contexts/accounts'
import Blogs from '../../contexts/blogs'
import connectToDB from '../../db/connect'
import { AccountUtils, BlogUtils } from '../__share__'


const create_user_data = AccountUtils.create_user_data()
const update_user_data = AccountUtils.update_user_data()
const invalid_user_data = AccountUtils.invalid_user_data()

const create_post_data = BlogUtils.create_post_data()
const update_post_data = BlogUtils.update_post_data()
const invalid_post_data = BlogUtils.invalid_post_data()

const create_comment_data = BlogUtils.create_comment_data()
const update_comment_data = BlogUtils.update_comment_data()
const invalid_comment_data = BlogUtils.invalid_comment_data()

const user_fixture = AccountUtils.user_fixture
const post_fixture = BlogUtils.post_fixture
const comment_fixture = BlogUtils.comment_fixture

describe('Context:Blogs', () => {

  beforeAll(async () => {
    await connectToDB('test')
  })

  beforeEach(async () => {
    await BlogUtils.clean()
  })

  it('get_posts', async () => {
    await post_fixture()
    const posts = await Blogs.get_posts()
  })

  it('get_post when the post exist : return user', async () => {
    const { _id } = await post_fixture('published')
    const post = await Blogs.get_post(_id)
    expect(post._id).to.not.undefined
  })

  it('create_post when the data is valid', async () => {
    const user_id = (await user_fixture())._id
    const post = await Blogs.create_post({...create_post_data, user_id})
    expect(post._id).to.not.undefined
  })

  it('update_post when the data is valid', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('', user_id))._id
    const post = await Blogs.update_post(post_id, {...update_post_data, user_id})
    expect(post._id).to.not.undefined
  })

  it('remove_post', async () => {
    const user_id = (await user_fixture())._id
    const { _id } = await post_fixture('', user_id)
    await Blogs.remove_post(_id, user_id)
    try {
      await Blogs.get_post(_id)
    } catch (e) {
      return;
    }

    throw new Error('test failed : remove_post with invalid data')
  })

  it('publish_post when data is valid', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('', user_id))._id
    const post = await Blogs.publish_post(post_id, user_id)
    expect(post.published_at).to.not.undefined
  })

  it('create_comment when the data is valid', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('published', user_id))._id

    const comment = await Blogs.create_comment({...create_comment_data, user_id, post_id})
    expect(comment._id).to.not.undefined
    expect(`${comment.user_creator._id}`).to.equal(`${user_id}`)
  })

  it('does not create_comment when the data is invalid', async () => {
    try {
      const user_id = (await user_fixture())._id
      const post_id = (await post_fixture('published', user_id))._id

      await BlogUtils.create_comment({...invalid_comment_data, user_id, post_id})
    } catch (e) {
      return;
    }
    throw new Error('test failed : create_user with invalid data')
  })

  it('update_comment when the data is valid', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('published', user_id))
    const comment_id = (await comment_fixture('', user_id, post_id))._id
    const comment = await Blogs.update_comment(comment_id, {...update_comment_data, user_id})
    expect(comment._id).to.not.undefined
  })

  it('does not update_comment when the data is invalid', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('published', user_id))
    const comment_id = (await comment_fixture('', user_id, post_id))._id
    try {
      await Blogs.update_comment(comment_id, {...invalid_comment_data, user_id})
    } catch (e) {
      return;
    }

    throw new Error('test failed : update_comment with invalid data')
  })

  it('remove_comment', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('published', user_id))
    const { _id } = await comment_fixture('', user_id, post_id)
    await Blogs.remove_comment(_id, user_id)
    try {
      await Blogs.get_comment(_id)
    } catch (e) {
      return;
    }

    throw new Error('test failed : remove_comment with invalid data')
  })
})
