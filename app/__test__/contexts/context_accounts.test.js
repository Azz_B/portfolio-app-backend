import { expect } from 'chai';
import Accounts from '../../contexts/accounts'
import connectToDB from '../../db/connect'
import { AccountUtils } from '../__share__'


const create_user_data = AccountUtils.create_user_data()
const update_user_data = AccountUtils.update_user_data()
const invalid_user_data = AccountUtils.invalid_user_data()

const user_fixture = AccountUtils.user_fixture

describe('Context:Accounts', () => {

  beforeAll(async () => {
    await connectToDB('test')
  })

  beforeEach(async () => {
    await AccountUtils.clean()
  })


  it('get_user when the user exist : return user', async () => {
    const { credential: {username} } = await user_fixture()
    const user = await Accounts.get_user(username)
  })

  it('create_user when the data is valid', async () => {
    const {_doc: user} = await Accounts.create_user(create_user_data)
    expect(user._id).to.not.undefined
    expect(user.credential.username).to.equal('some_username')
    expect(user.credential.email).to.equal('some@email.com')
    expect(user.credential.password).to.not.equal('some_password')
    expect(user.profile.fullname).to.equal('some_fullname')
  })

  it('does not create_user when the data is invalid', async () => {
    try {
      await Accounts.create_user(invalid_user_data)
    } catch (e) {
      return;
    }
    throw new Error('test failed : create_user with invalid data')
  })

  it('update_user when data is invalid', async () => {
    const user_id = (await AccountUtils.user_fixture())._id
    const user = await Accounts.update_user(user_id, update_user_data)

    expect(`${user_id}`).to.equal(`${user._id}`)
    expect(user.profile.fullname).to.equal('updated_some_fullname')
  })

  // it('remove_user', async () => {
  //   const user_id = (await AccountUtils.user_fixture())._id
  //   await Accounts.remove_user(user_id)
  //   try {
  //     await Accounts.get_user(user_id)
  //   } catch (e) {
  //     return;
  //   }
  //
  //   throw new Error('test failed : remove_user')
  // })
})
