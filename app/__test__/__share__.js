import Accounts from '../contexts/accounts'
import Blogs from '../contexts/blogs'
import Portfolios from '../contexts/portfolios'

const create_user_data = { username: 'some_username', email: 'some@email.com', password: 'some_password', fullname: 'some_fullname' }
const update_user_data = {fullname: 'updated_some_fullname', bio: 'updated_some_bio' }
const invalid_user_data = { email: '', password: '', fullname: ''}

const create_post_data = {title_draft: 'some_title', subtitle_draft: 'some_subtitle', content_draft: 'some_content' }
const update_post_data = {title_draft: 'updated_some_title', subtitle_draft: 'updated_some_subtitle', content_draft: 'updated_some_content' }
const invalid_post_data = {}

const create_comment_data = {content: 'some_content'}
const update_comment_data = {content: 'updated_some_content'}
const invalid_comment_data = {}

const create_portfolio_data = {intro: 'some_intro'}
const update_portfolio_data = {intro: 'updated_some_intro'}
const invalid_portfolio_data = {}


function get(model, flag) {
  return (flag === 'doc')? model._doc : model
}

async function get_user_id(user_id) {
  return !user_id ? (await AccountUtils.user_fixture('doc'))._id : user_id
}

async function get_post_id(post_id, user_id) {
  return !post_id ? (await BlogUtils.post_fixture('doc', user_id))._id : post_id
}

function assign(key, base_data) {
  return (data = {}) => ({[key]: {...base_data, ...data }})
}

export const AccountUtils = {
  create_user_data: assign('user', create_user_data),
  update_user_data: assign('user', update_user_data),
  invalid_user_data: assign('user', invalid_user_data),

  user_fixture: async (flag, data) => {
    const user = await Accounts.create_user(AccountUtils.create_user_data(data))
    return get(user, flag)
  },

  futured_user: async () => {

  },

  clean: async () => {
    await Accounts.clean()
  },
}


export const BlogUtils = {
  create_post_data: assign('post', create_post_data),
  update_post_data: assign('post', update_post_data),
  invalid_post_data: assign('post', invalid_post_data),

  post_fixture: async (flag, user_id) => {
    user_id = await get_user_id(user_id)
    let post = await Blogs.create_post({...BlogUtils.create_post_data(), user_id})
    if(flag === 'published') {
      post.published_at = Date.now();
      await post.save()
    }
    return get(post, flag)
  },

  create_comment_data: assign('comment', create_comment_data),
  update_comment_data: assign('comment', update_comment_data),
  invalid_comment_data: assign('comment', invalid_comment_data),

  comment_fixture: async (flag, user_id, post_id) => {
    user_id = await get_user_id(user_id)
    post_id = await get_post_id(post_id, user_id)
    const comment = await Blogs.create_comment({...BlogUtils.create_comment_data(), user_id, post_id})
    return get(comment, flag)
  },

  clean: async () => {
    await Blogs.clean()
  },
}


export const PortfolioUtils = {
  create_portfolio_data: assign('portfolio', create_comment_data),
  update_portfolio_data: assign('portfolio', update_portfolio_data),
  invalid_portfolio_data: assign('portfolio', invalid_portfolio_data),

  portfolio_fixture: async (flag, user_id) => {
    user_id = await get_user_id(user_id)
    const portfolio = await Blogs.create_portfolio({...PortfolioUtils.create_portfolio_data(), user_id})
    return get(portfolio, flag)
  },

  clean: async () => {
    await Portfolios.clean()
  },
}
