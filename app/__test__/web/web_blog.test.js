import supertest from 'supertest';
import { expect } from 'chai';
import server from './__test_server__'
import { send_request, login_process } from './__helpers__'
import { AccountUtils, BlogUtils } from '../__share__'

const prefix = '/api_v1/posts'

const create_post_data = BlogUtils.create_post_data()
const update_post_data = BlogUtils.update_post_data()
const invalid_post_data = BlogUtils.invalid_post_data()

const user_fixture = AccountUtils.user_fixture
const post_fixture = BlogUtils.post_fixture
const comment_fixture = BlogUtils.comment_fixture


describe('Web:Blog', () => {
  let request, logged_user, token

  beforeAll(async () => {
    request = supertest(server.listen())
    const result = await login_process(request)
    token = result.token
    logged_user = result.user
  })

  beforeEach(async () => {
    await BlogUtils.clean()
  })

  it('GET /api_v1/posts without token | return the posts ', async () => {
    const user_id = (await user_fixture())._id
    const id = (await post_fixture('', user_id))._id
    const { body } = await  send_request(request, 'get', `${prefix}?limit=${10}&offset=${0}`)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body);
    expect(body).to.not.undefined
  })

  it('GET /api_v1/posts with token | return the posts ', async () => {
    const user_id = (await user_fixture())._id
    const id = (await post_fixture('', user_id))._id
    const { body } = await  send_request(request, 'get', `${prefix}`, token)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body);
    expect(body).to.not.undefined
  })

  it('GET /api_v1/posts/:id | return the post', async () => {
    const user_id = (await user_fixture())._id
    const id = (await post_fixture('published', user_id))._id
    const { body } = await  send_request(request, 'get', `${prefix}/${id}`)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body);
    expect(body['post']).to.not.undefined
    const post = body['post']
    expect(post.id).to.equal(`${id}`)
    expect(post.user.id).to.equal(`${user_id}`)
    //expect(post.content).to.not.undefined
  })

  it('POST /api_v1/posts with token | return post', async () => {
    const { body } = await  send_request(request, 'post', `${prefix}`, create_post_data, token)
                              .expect('Content-Type', /json/)
                              .expect(200)

    expect(body['post']).to.not.undefined
    //console.log(body);
  })

  it('POST /api_v1/posts without token | return errors', async () => {
    const { body } = await  send_request(request, 'post', `${prefix}`, create_post_data)
                              .expect('Content-Type', /json/)
                              .expect(401)

    expect(body['reason']).to.equal('Action not Authorized')
  })

  it('POST /api_v1/posts without token | return errors', async () => {
    const { body } = await  send_request(request, 'post', `${prefix}`, invalid_post_data, token)
                              .expect('Content-Type', /json/)
                              .expect(400)

    expect(body['reason']).to.not.undefined
    //console.log(body);
  })

  it('PUT /api_v1/posts/:id with token | return post', async () => {
    const id = (await post_fixture('', logged_user.id))._id
    const { body } = await  send_request(request, 'put', `${prefix}/${id}`, update_post_data, token)
                              .expect('Content-Type', /json/)
                              .expect(200)

    expect(body['post']).to.not.undefined
    //console.log(body);
  })

  it('PUT /api_v1/posts/:id without token | return errors', async () => {
    const id = (await post_fixture('', logged_user.id))._id
    const { body } = await  send_request(request, 'put', `${prefix}/${id}`, update_post_data)
                              .expect('Content-Type', /json/)
                              .expect(401)

    expect(body['reason']).to.equal('Action not Authorized')
  })

  it('PUT /api_v1/posts/:id with invalid data | return errors', async () => {
    const id = (await post_fixture('', logged_user.id))._id
    const { body } = await send_request(request, 'put', `${prefix}/${id}`, invalid_post_data, token)
                            .expect('Content-Type', /json/)
                            .expect(400)

    //console.log(body);
  })

  it('POST /api_v1/posts/:id/publish  | return post', async () => {
    const id = (await post_fixture('', logged_user.id))._id
    const { body } = await send_request(request, 'post', `${prefix}/${id}/publish`, {}, token)
                            .expect('Content-Type', /json/)
                            .expect(200)
    console.log(body);
  })

  it('GET /api_v1/posts/:id/comments | return the post comments', async () => {
    const user_id = (await user_fixture())._id
    const post_id = (await post_fixture('published', user_id))._id
    const id = (await comment_fixture('', user_id, post_id))._id

    const { body } = await  send_request(request, 'get', `${prefix}/${post_id}/comments`)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body['data']);
    expect(body['comments']).to.not.undefined
    const comment = body['comments'][0]
    expect(comment.id).to.equal(`${id}`)
    expect(comment.user.id).to.equal(`${user_id}`)
  })
})
