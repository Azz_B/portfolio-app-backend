import Koa from 'koa'
import koaBody from 'koa-body'
import CustomError from '../../lib/CustomError'
import router from '../../web/routes'
import connectToDB from '../../db/connect'

require('dotenv').config();

const koa = new Koa()

connectToDB('test')


koa.use(koaBody())
koa.use(router.routes())


if (!module.parent) {
  const PORT = process.env.PORT_TEST || 4001
  var server = koa.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
  });
}


global.LOG = (message, ...args) => {
  if(true) return;

  console.log('/ ---------------'+ message +'------------------ ');
  console.log(...args);
  console.log(' ----------------------------------------------- / ');
}


export default koa
