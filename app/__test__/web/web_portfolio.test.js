import supertest from 'supertest';
import { expect } from 'chai';
import server from './__test_server__'
import { send_request } from './__helpers__'
import { AccountUtils } from '../__share__'

const prefix = '/api_v1/portfolios'

const user_fixture = AccountUtils.user_fixture


describe('Web:Portfolio', () => {
  let request

  beforeAll(() => {
    request = supertest(server.listen())
  })

  beforeEach(async () => {

  })

  it('GET /api_v1/portfolios/main | return the Portfolio of the me "admin" :)', async () => {
    const { body } = await  send_request(request, 'get', `${prefix}/main`)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body['data']);
    expect(body['portfolio']).to.not.undefined
    const portfolio = body['portfolio']
    expect(portfolio['status']).to.not.undefined
    expect(portfolio['user']).to.not.undefined
  })

})
