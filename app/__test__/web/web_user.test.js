import supertest from 'supertest';
import { expect } from 'chai';
import server from './__test_server__'
import { send_request, login_process } from './__helpers__'
import { AccountUtils, BlogUtils } from '../__share__'

const prefix = '/api_v1/users'

const update_user_data = AccountUtils.update_user_data()
const invalid_user_data = AccountUtils.invalid_user_data()

const user_fixture = AccountUtils.user_fixture
const post_fixture = BlogUtils.post_fixture

describe('Web:User', () => {
  let request

  beforeAll(async () => {
    request = supertest(server.listen())
  })

  beforeEach(async () => {
    await AccountUtils.clean()
  })

  it('GET /api_v1/users/:username | return the user', async () => {
    const username = (await user_fixture()).credential.username
    const { body } = await  send_request(request, 'get', `${prefix}/${username}`)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body);
    const user = body['user']
    expect(user['username']).to.equal(username)
  })

  it('PUT /api_v1/users | update the user and return it', async () => {
    await user_fixture()
    const result = await login_process(request, { email: 'some@email.com', password: 'some_password'})
    const { body } = await send_request(request, 'put', prefix, update_user_data, result.token)
                               .expect('Content-Type', /json/)
                               .expect(200)
    //console.log(body);
  })

  it('GET /api_v1/users/:username/posts | return the user posts', async () => {
    const user = await user_fixture()
    const post = await post_fixture('', user._id)
    const username = user.credential.username
    const { body } = await  send_request(request, 'get', `${prefix}/${username}/posts`)
                                  .expect('Content-Type', /json/)
                                  .expect(200)
    //console.log(body['data']['posts']);
    //expect(body['data']).to.not.undefined
  })


})
