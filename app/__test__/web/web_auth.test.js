import supertest from 'supertest';
import { expect } from 'chai';
import server from './__test_server__'
import { send_request, login_process } from './__helpers__'
import { AccountUtils } from '../__share__'

const prefix = '/auth'

const create_user_data = AccountUtils.create_user_data()
const invalid_user_data = AccountUtils.invalid_user_data()
const user_fixture = AccountUtils.user_fixture


describe('Web:Auth', () => {
  let request

  beforeAll(async () => {
    request = supertest(server.listen())
  })

  beforeEach(async () => {
    await AccountUtils.clean()
  })


  it('POST /auth/register with valid data - return user', async () => {
    const { body } = await send_request(request, 'post', `${prefix}/register`, create_user_data)
                            .expect('Content-Type', /json/)
                            .expect(200)

    expect(body['user']).to.not.undefined
    expect(body['token']).to.not.undefined
    //console.log(body);
  })

  it('POST /auth/register with an existed email - return errors', async () => {
    await user_fixture()
    const { body } = await send_request(request, 'post', `${prefix}/register`, create_user_data)
                            .expect('Content-Type', /json/)
                            .expect(400)

    expect(body['reason']).to.not.undefined
    expect(body['errors']).to.not.undefined
    //console.log(body);
    //expect(body['errors']['email']).to.equal('email is already exist')

  })

  it('POST /auth/register with invalid data - return errors', async () => {
    const { body } = await send_request(request, 'post', `${prefix}/register`, invalid_user_data)
                            .expect('Content-Type', /json/)
                            .expect(400)

    expect(body['reason']).to.not.undefined
    expect(body['errors']).to.not.undefined
    //console.log(body);
    //expect(body['errors']['email']).to.equal('email has an invalid format')
  })

  it('POST /auth/login with valid data - return user', async () => {
    const user = await user_fixture()
    const { body } = await send_request(request, 'post', `${prefix}/login`, {login: 'some_username', password: 'some_password'})
                            .expect('Content-Type', /json/)
                            .expect(200)

    expect(body['user']).to.not.undefined
    expect(body['token']).to.not.undefined
    //console.log(body);
  })

  it('POST /auth/login with invalid data - return erros', async () => {
    await user_fixture()
    const { body } = await send_request(request, 'post', `${prefix}/login`, {login: 'invalid@email.com', password: 'invalid_password'})
                            .expect('Content-Type', /json/)
                            .expect(401)

    expect(body['reason']).to.equal("Authentication failed")
    expect(body['errors']).to.not.undefined
    //console.log(body);
  })

  it('GET /auth/login_by_token with valid token - return user', async () => {
    const {token} = await login_process(request)
    const { body } = await send_request(request, 'get', `${prefix}/login_by_token`)
                            .set('x-access-token', token)
                            .expect('Content-Type', /json/)
                            .expect(200)
    //console.log(body);
  })

  it('GET /auth/login_by_token with invalid token - return errors', async () => {
    const { body } = await send_request(request, 'get', `${prefix}/login_by_token`)
                            .set('x-access-token', 'dfd.dfdf.dfdf')
                            .expect('Content-Type', /json/)
                            .expect(404)
    //console.log(body);
  })
})
